package com.example.assignment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Button;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Activity2 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Intent browserIntent;

        //noinspection SimplifiableIfStatement
        if (id == R.id.go_to_googleMaps) {

            browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/maps"));
            startActivity(browserIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Intent intent;

        if (id == R.id.nav_home) {

            intent = new Intent(this,Activity2.class); //change to respective activity
            startActivity(intent);

        } else if (id == R.id.nav_info) {


        } else if (id == R.id.firebase) {

            intent = new Intent(this,firebase.class); //change to respective activity
            startActivity(intent);


        } else if (id == R.id.nav_share) {


        } else if (id == R.id.nav_contactus) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void exitApp(View view)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        moveTaskToBack(true);
                    }
                })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    //method to open the google maps activity
    public void openMapsActivity(View view)
    {
        Toast.makeText(Activity2.this,"Fetching Location", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, GoogleMapsActivity.class);
        startActivity(intent);

    }

    /*
    String formattedDate;
        @Override
        protected void onStart() {
            super.onStart();

            // Retrieve data
            setContentView(R.layout.activity_2);
            View parentLayout = findViewById(android.R.id.content);
            Snackbar.make(parentLayout, formattedDate, Snackbar.LENGTH_LONG).setAction("Close", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        }).show();
    }
    @Override
    protected void onResume() {
        super.onResume();
        // Retrieve data
        setContentView(R.layout.activity_2);
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, formattedDate, Snackbar.LENGTH_LONG).setAction("Close", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        }).show();
    }

    @Override
    protected void onStop() {
        super.onStop();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Activity2.this);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString("time", formattedDate);
        Calendar ca = Calendar.getInstance();
        SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = d.format(ca.getTime());
    }


    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Activity2.this);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString("time", formattedDate);
        Calendar ca = Calendar.getInstance();
        SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = d.format(ca.getTime());
    }
    */

}
