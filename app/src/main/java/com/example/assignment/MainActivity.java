package com.example.assignment;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.nightonke.blurlockview.BlurLockView;
import com.nightonke.blurlockview.Directions.HideType;
import com.nightonke.blurlockview.Eases.EaseType;
import com.nightonke.blurlockview.Password;

public class MainActivity extends AppCompatActivity {

    private BlurLockView blurLockView;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        blurLockView = (BlurLockView)findViewById(R.id.blurLockView);
        blurLockView.setBlurredView(imageView);

        imageView = (ImageView)findViewById(R.id.background);

        blurLockView.setCorrectPassword("1406");
        blurLockView.setLeftButton("");
        blurLockView.setRightButton("");
        blurLockView.setTypeface(Typeface.DEFAULT);
        blurLockView.setType(Password.NUMBER,false);

        blurLockView.setOnPasswordInputListener(new BlurLockView.OnPasswordInputListener() {
            @Override
            public void correct(String inputPassword) {
                Toast.makeText(MainActivity.this,"Password Correct", Toast.LENGTH_SHORT).show();
                blurLockView.hide(1000, HideType.FADE_OUT, EaseType.EaseInBounce);
                openActivity2();
            }

            @Override
            public void incorrect(String inputPassword) {
                Toast.makeText(MainActivity.this,"Password Incorrect", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void input(String inputPassword) {
                //
            }

        });

    }

    //method to open second activity
    public void openActivity2()
    {
        Intent intent = new Intent(this, Activity2.class);
        startActivity(intent);
    }

}
